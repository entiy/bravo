这是项目 [bravo](https://github.com/Entiy/Test) ，
欢迎访问。

这个项目的版本库是 **Git格式** ，在 Windows、Linux、Mac OS X
平台都有客户端工具可以访问。虽然版本库只提供Git一种格式，
但是你还是可以用其他用其他工具访问，如 ``svn`` 和 ``hg`` 。

## 版本库地址

支持三种访问协议：

* HTTP协议: `https://github.com/Entiy/Test.git` 。
* Git协议: `git://github.com/Entiy/Test.git` 。
* SSH协议: `ssh://git@github.com/Entiy/Test.git` 。

## 克隆版本库

操作示例：

    $ git clone git://github.com/Entiy/Test.git

